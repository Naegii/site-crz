
$(".menu").click(function(){
$(".menu-open").slideDown();
$("#menuop").css('display','none');
$("#menu-close").css('display','block');
$(".nav").css('background-color','rgba(11, 66, 121, 0.8)');
});


$("#menu-close").click(function(){
$(".menu-open").slideUp();
$("#menuop").css('display','block');
$("#menu-close").css('display','none');
$(".nav").css('background-color','rgba(11, 66, 121, 1)');
    
}); 

$(".study-click").click(function(e){

//$(this).parent().find("div.study-more").slideToggle('slow'); 
    
if($(this).html() === 'Ler mais <img src="img/lermais.png">'){
    e.preventDefault();
        $(this).closest(".study-card").find("ul").attr("class","height_auto"); 
    $(this).parent().find("div.study-more").fadeOut().slideToggle('slow',function(){
        
    }); 
    $(this).html('Ler menos <img src="img/lermenos.png">');
    
}     else {
    $(this).parent().find("div.study-more").slideToggle('slow',function(){
    $(this).closest(".study-card").find("ul").attr("class","height_200"); 
        $(this).closest(".study-card").find("ul").attr("class","height_200"); 
        var target_offset = $(this).closest('.study-card').offset();
        var target_top = target_offset.top;
        $('html, body').animate({ scrollTop: target_top }, 0);
        
    });   
    $(this).html('Ler mais <img src="img/lermais.png">'); 
    
}
    

});

$(".mani-click").click(function(){
$(this).parent().find("div.mani-more").slideToggle('slow'); 
$(this).html() === 'Ler mais <img src="img/lermais.png">' ? $(this).html('Ler menos <img src="img/lermenos.png">') : $(this).html('Ler mais <img src="img/lermais.png">');  
});



$('.depoimentos').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev"><img src="img/prevw.png"></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="img/nextw.png"></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                },
            ]
        });
$('.dna').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev-dna"><img src="img/prev.png"></button>',
            nextArrow: '<button type="button" class="slick-next-dna"><img src="img/next.png"></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                },
            ]
        });
$('.aprendizagem').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev-apr"><img src="img/prev.png"></button>',
            nextArrow: '<button type="button" class="slick-next-apr"><img src="img/next.png"></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                },
            ]
        });
$('.banner').slick({
            infinite: true,
            autoplay: true,
            arrows: false,
            dots: true,
            speed: 750,

        });



/* FUNÇÃO DO MENU
$( document ).ready(function() {
   $('.nav').removeClass('hide');
var lastScrollTop = 0;
$(window).scroll(function(){
  var st = $(this).scrollTop();
    console.log(st);
  var banner = $('.nav');
  setTimeout(function(){
    if (st > lastScrollTop){
      banner.addClass('hide');
    } else {
      banner.removeClass('hide');
    }
    lastScrollTop = st;
  }, 100);
    
    
});    
});*/


// Função Destinada ao Slick. 

// Mobile
if (window.matchMedia("(max-width: 480px)").matches){
    $('.studies').slick({
            slidesToShow: 1,
            dots: true,
            arrows: false,
            infinite: false,
            speed: 750,

  });
    $('.emp').slick({
        slidesToShow: 1,
        dots: true,
        arrows: false,
        infinite: false,
        speed: 750,
    });    
    $('.tipos').slick({
            slidesToShow: 1,
            dots: true,
            arrows: false,
            infinite: false,
            speed: 750,
    });    
} 
// Tablet

else if (window.matchMedia("(max-width: 768px)").matches){
    
    $('.studies').slick({
           slidesToShow: 2,
                        dots: true,
                        arrows: false,
      infinite: false,
speed: 750,
      
  });
    $('.emp').slick({
        slidesToShow: 2,
        dots: true,
        arrows: false,
        infinite: false,
        speed: 750,
    });   
    $('.tipos').slick({
        slidesToShow: 2,
        dots: true,
        arrows: false,
        infinite: false,
        speed: 750,
    });  
    $('.content4').slick('unslick');
} 
// Desktop Slicks

else {
    
$('.content4').slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev-apr"><img src="img/prev.png"></button>',
            nextArrow: '<button type="button" class="slick-next-apr"><img src="img/next.png"></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 1,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 1,
                        dots: false,
                        arrows: true
                    }
                },
        
            ]
        });
    
    
}

// Função criada para Ajuste dots do studies. 
$(".studies").on('beforeChange', function(event, slick, currentSlide) {
    event.preventDefault();
    $('.studies').find(".slick-dots").fadeOut();
});

$(".studies").on('afterChange', function(event, slick, currentSlide) {
    
    $('.studies').find(".slick-dots").fadeIn();
    event.preventDefault();
    var c = $(".studies").find(".slick-track .slick-active").length;
    var b = $('.studies').find(".slick-active").height() + 100;
    
    if(c > 1){
        console.log('entrou aqui');
        for (i = 0; i <= c; i++) {
            var height_calc = $(".studies").find(".slick-track .slick-active").eq(i).height();
            if (b < height_calc){
                b = height_calc + 100;
            }
        }
    }
    
    var b_index = $('.studies').find(".slick-active").attr("data-slick-index");
   $('.studies').find(".slick-list ").attr("height",b+'px');
   $('.studies').closest(".slick-initialized").attr("style","height:"+b+'px!important;');
});

$('.studies').on('init', function(event, slick){
            // let's do this after we init the banner slider

            console.log('slider was initialized');
        })


$(document).on("click","#top_arrow",function(e){
        var target_offset = $(".nav").offset();
    
        var target_top = target_offset.top;
        $('html, body').animate({ scrollTop: target_top }, 0);
})
